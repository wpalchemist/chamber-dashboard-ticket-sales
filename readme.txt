=== Chamber Dashboard Ticket Sales ===
Contributors: gwendydd
Tags: events, event calendar, event tickets, ticketing, tickets, ticket sales, chamber of commerce
Donate link: http://chamberdashboard.com/donate
Requires at least: 3.8
Tested up to: 4.2.2
Stable tag: trunk
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Works with the Chamber Dashboard Events Calendar to sell tickets to your events.

== Description ==
Long plugin description

= Feature List:  =
*   feature 1
*   feature 2

You can learn more at [Chamber Dashboard](http://chamberdashboard.com)



== Installation ==
= Using The WordPress Dashboard =

1. Navigate to the \'Add New\' in the plugins dashboard
2. Search for \'WPA Starter Plugin\'
3. Click \'Install Now\'
4. Activate the plugin on the Plugin dashboard

= Uploading in WordPress Dashboard =

1. Navigate to the \'Add New\' in the plugins dashboard
2. Navigate to the \'Upload\' area
3. Select `wpa-starter-plugin.zip` from your computer
4. Click \'Install Now\'
5. Activate the plugin in the Plugin dashboard

= Using FTP =

1. Download `wpa-starter-plugin.zip`
2. Extract the `wpa-starter-plugin` directory to your computer
3. Upload the `wpa-starter-plugin` directory to the `/wp-content/plugins/` directory
4. Activate the plugin in the Plugin dashboard


== Frequently Asked Questions ==
= Question? =
Answer


== Screenshots ==
1. Screenshot title

== Changelog ==

= 1.0 =
* First release