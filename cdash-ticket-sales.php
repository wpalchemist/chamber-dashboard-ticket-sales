<?php
/*
Plugin Name: Chamber Dashboard Ticket Sales
Plugin URI: http://chamberdashboard.com
Description: Basic setup to start a plugin
Version: 1
Author: Morgan Kay
Author URI: http://chamberdashboard.com
Text Domain: cdashts
*/

/*  Copyright 2015 Morgan Kay (email : morgan@chamberdashboard.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// ------------------------------------------------------------------------
// REQUIREMENTS                                               
// ------------------------------------------------------------------------

// minimum WordPress version
function cdashts_requires_wordpress_version() {
	global $wp_version;
	$plugin = plugin_basename( __FILE__ );
	$plugin_data = get_plugin_data( __FILE__, false );

	if ( version_compare($wp_version, "3.8", "<" ) ) {
		if( is_plugin_active($plugin) ) {
			deactivate_plugins( $plugin );
			wp_die( "'".$plugin_data['Name']."' requires WordPress 3.8 or higher, and has been deactivated! Please upgrade WordPress and try again.<br /><br />Back to <a href='".admin_url()."'>WordPress admin</a>." );
		}
	}
}
add_action( 'admin_init', 'cdashts_requires_wordpress_version' );

// require Events Calendar
add_action( 'admin_init', 'cdashts_require_events_calendar' );
function cdashts_require_events_calendar() {
    $cdashec_info = get_plugin_data( WP_PLUGIN_DIR . '/chamber-dashboard-events-calendar/cdash-event-calendar.php', $markup = false, $translate = false );
    if ( is_admin() && current_user_can( 'activate_plugins' ) && ( !is_plugin_active( 'chamber-dashboard-events-calendar/cdash-event-calendar.php' ) || '1.3.1' >= $cdashec_info['Version'] ) ) {
        add_action( 'admin_notices', 'cdashts_events_calendar_notice' );

        deactivate_plugins( plugin_basename( __FILE__ ) ); 

        if ( isset( $_GET['activate'] ) ) {
            unset( $_GET['activate'] );
        }
    }
}

function cdashts_events_calendar_notice(){
    ?><div class="error"><p><?php _e( 'Sorry, but Chamber Dashboard Ticket Sales requires the <a href="https://wordpress.org/plugins/chamber-dashboard-events-calendar/" target="_blank">Chamber Dashboard Events Calendar</a>, version 1.4 or higher, to be installed and active.  Please install or update Chamber Dashboard Events Calendar.', 'cdashrp' ); ?></p></div><?php
}


// ------------------------------------------------------------------------
// REGISTER HOOKS & CALLBACK FUNCTIONS:
// ------------------------------------------------------------------------

// Set-up Action and Filter Hooks
// register_activation_hook(__FILE__, 'cdashts_add_defaults');
// register_uninstall_hook(__FILE__, 'cdashts_delete_plugin_options');
// add_action('admin_init', 'cdashts_init' );
// add_action('admin_menu', 'cdashts_add_options_page');
// add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'cdashts_plugin_action_links', 10, 2 );

// Require options stuff
// require_once( plugin_dir_path( __FILE__ ) . 'options.php' );
// Require views
require_once( plugin_dir_path( __FILE__ ) . 'views.php' );


// Initialize language so it can be translated
function cdashts_language_init() {
  load_plugin_textdomain( 'cdashts', false, dirname(plugin_basename(__FILE__)) . 'languages' );
}
add_action('init', 'cdashts_language_init');


// ------------------------------------------------------------------------
// CREATE CUSTOM POST TYPE FOR TICKETS
// ------------------------------------------------------------------------

// Register Custom Post Type
function cdashts_create_ticket_cpt() {

    $labels = array(
        'name'                => _x( 'Tickets', 'Post Type General Name', 'cdashts' ),
        'singular_name'       => _x( 'Ticket', 'Post Type Singular Name', 'cdashts' ),
        'menu_name'           => __( 'Tickets', 'cdashts' ),
        'name_admin_bar'      => __( 'Tickets', 'cdashts' ),
        'parent_item_colon'   => __( 'Parent Ticket:', 'cdashts' ),
        'all_items'           => __( 'All Tickets', 'cdashts' ),
        'add_new_item'        => __( 'Add New Ticket', 'cdashts' ),
        'add_new'             => __( 'Add New', 'cdashts' ),
        'new_item'            => __( 'New Ticket', 'cdashts' ),
        'edit_item'           => __( 'Edit Ticket', 'cdashts' ),
        'update_item'         => __( 'Update Ticket', 'cdashts' ),
        'view_item'           => __( 'View Ticket', 'cdashts' ),
        'search_items'        => __( 'Search Tickets', 'cdashts' ),
        'not_found'           => __( 'Not found', 'cdashts' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'cdashts' ),
    );
    $rewrite = array(
        'slug'                => 'post_type',
        'with_front'          => true,
        'pages'               => false,
        'feeds'               => false,
    );
    $args = array(
        'label'               => __( 'event_ticket', 'cdashts' ),
        'description'         => __( 'Ticket for an event', 'cdashts' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-tickets-alt',
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => false,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'rewrite'             => $rewrite,
        'capability_type'     => 'page',
    );
    register_post_type( 'event_ticket', $args );

}

add_action( 'init', 'cdashts_create_ticket_cpt', 0 );

// Register Ticket Questionnaire
function cdashts_create_questionnaire_cpt() {

    $labels = array(
        'name'                => _x( 'Questionnaires', 'Post Type General Name', 'cdashts' ),
        'singular_name'       => _x( 'Questionnaire', 'Post Type Singular Name', 'cdashts' ),
        'menu_name'           => __( 'Questionnaire', 'cdashts' ),
        'name_admin_bar'      => __( 'Questionnaire', 'cdashts' ),
        'parent_item_colon'   => __( 'Parent Questionnaire:', 'cdashts' ),
        'all_items'           => __( 'Questionnaires', 'cdashts' ),
        'add_new_item'        => __( 'Add New Questionnaire', 'cdashts' ),
        'add_new'             => __( 'Add New', 'cdashts' ),
        'new_item'            => __( 'New Questionnaire', 'cdashts' ),
        'edit_item'           => __( 'Edit Questionnaire', 'cdashts' ),
        'update_item'         => __( 'Update Questionnaire', 'cdashts' ),
        'view_item'           => __( 'View Questionnaire', 'cdashts' ),
        'search_items'        => __( 'Search Questionnaires', 'cdashts' ),
        'not_found'           => __( 'Not found', 'cdashts' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'cdashts' ),
    );
    $args = array(
        'label'               => __( 'questionnaire', 'cdashts' ),
        'description'         => __( 'Questions to be asked during ticket purchase', 'cdashts' ),
        'labels'              => $labels,
        'supports'            => array( 'title', ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => 'edit.php?post_type=event_ticket',
        'menu_position'       => 5,
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => false,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'capability_type'     => 'page',
    );
    register_post_type( 'questionnaire', $args );

}

add_action( 'init', 'cdashts_create_questionnaire_cpt', 0 );

if( class_exists( 'WPAlchemy_MetaBox' ) ) {
    // Create metabox for questionnaires
    $membership_metabox = new WPAlchemy_MetaBox(array
    (
        'id' => 'questionnaire_meta',
        'title' => 'Ticket Questions',
        'types' => array('questionnaire'),
        'template' => plugin_dir_path( __FILE__ ) . '/includes/questionnaire_meta.php',
        'mode' => WPALCHEMY_MODE_EXTRACT,
        'prefix' => '_cdashts_',
        'context' => 'normal',
        'priority' => 'high'
    ));
}

// ------------------------------------------------------------------------
// LINK TICKETS TO EVENTS AND TO PEOPLE
// ------------------------------------------------------------------------

if( defined( 'CDASH_PATH' ) ) {
    // Create the connection between businesses and invoices
    function cdashts_tickets_to_events() {
        p2p_register_connection_type( array(
            'name' => 'tickets_to_events',
            'from' => 'event_ticket',
            'to' => 'event',
            'cardinality' => 'many-to-one',
            'admin_column' => 'from',
            'to_query_vars' => array('post_status' => 'any')
        ) );
    }
    add_action( 'p2p_init', 'cdashts_tickets_to_events' );

    if( defined( 'CDCRM_PATH' ) ) {
        function cdashts_tickets_to_people() {
            p2p_register_connection_type( array(
                'name' => 'tickets_to_people',
                'from' => 'event_ticket',
                'to' => 'person',
                'cardinality' => 'many-to-many',
                'admin_column' => 'from',
                'to_query_vars' => array('post_status' => 'any')
            ) );
        }
        add_action( 'p2p_init', 'cdashts_tickets_to_people' );
    }
}

// ------------------------------------------------------------------------
// TICKET METABOX
// ------------------------------------------------------------------------

function cdashts_ticket_meta_fields($post) {
    global $post;

    $sales_location = get_post_meta($post->ID, '_sales_location', true);
    $tix_sales_open = explode(' ', get_post_meta($post->ID, '_tix_sales_open', true));
    $tix_sales_close = explode(' ', get_post_meta($post->ID, '_tix_sales_close', true));
    $display = "none";
    if( isset( $sales_location ) && "sell_here" == $sales_location ) {
        $display = "block";
    }

    // get a list of the questionnaires
    $args = array( 
        'post_type' => 'questionnaire',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'title',
    );

    $options = '';
    
    $questions = new WP_Query( $args );
    if ( $questions->have_posts() ) :
        while ( $questions->have_posts() ) : $questions->the_post();
            $options .= '<option value="' . get_the_id() . '">' . get_the_title() . '</option>';
        endwhile;
    endif;
    wp_reset_postdata();   


    $html = '
    <div class="sales-location-row">
        '. __( 'Where do you want to sell tickets?', 'cdashts' ) . '<br />
        <input id="sell_here" value="sell_here" type="radio" class="sales_location" name="sales_location" '.checked($sales_location, 'sell_here', false).' />
        <label for="sell_here">'.__(' Sell tickets on this site', 'cdashts').'</label><br />
        <input id="sell_external" value="sell_external" type="radio" class="sales_location" name="sales_location" '.checked($sales_location, 'sell_external', false).' />
        <label for="sell_external">'.__(' Sell tickets on an external site', 'cdashts').'</label><br />
    </div>

    <div id="ticket-sales-options" style="display:' . $display . '">
        <h4>' . __( 'Ticket Sales Options', 'cdashts' ) . '</h4>
        <div class="tix-number">
            <label for="tix-number">' . __( 'Number of tickets available', 'cdashts' ) . ': </label>
            <input id="tix-number" type="number" name="tix_number" value="'.get_post_meta($post->ID, '_tix_number', true).'" />
        </div>

        <div class="ticket-sales-dates">
            <div class="ticket-sales-open">
                <label for="tix_sales_open">'.__('Date/time ticket sales open', 'cdashts').':</label> <input id="tix_sales_open" class="format-date" type="text" name="tix_sales_open" value="'.esc_attr($tix_sales_open[0]).'"/> <input id="tix_sales_start_time" class="format-time" type="text" name="tix_sales_start_time" value="'.esc_attr(isset($tix_sales_open[1]) ? substr($tix_sales_open[1], 0, 5) : '').'"/>
            </div>
            <div class="ticket-sales-close">
                <label for="tix_sales_close">'.__('Date/time ticket sales close', 'cdash-events').':</label> <input id="tix_sales_close" class="format-date" type="text" name="tix_sales_close" value="'.esc_attr($tix_sales_close[0]).'"/> <input id="tix_sales_close_time" class="format-time" type="text" name="tix_sales_close_time" value="'.esc_attr(isset($tix_sales_close[1]) ? substr($tix_sales_close[1], 0, 5) : '').'"/>
            </div>
        </div>

        <div class="ticket-sales-questionnaire">
            <label>Ticket Questionnaire</label>
            <select name="questionnaire" id="questionnaire">
                <option value=""> </option>';
                if( isset( $options ) && '' !== $options ) {
                    $html .= $options;
                }

            $html .= '
            </select>
            <p>' . __( 'To make ticket questionnaires, go to Tickets --> Questionnaires and click "Add New"', 'cdashts' ) . '</p>
        </div>

    </div>';

    echo $html;
}

add_action('cde_after_metabox_event_tickets', 'cdashts_ticket_meta_fields');

?>
