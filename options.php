<?php
/* Options Page */

// --------------------------------------------------------------------------------------
// CALLBACK FUNCTION FOR: register_uninstall_hook(__FILE__, 'wpasp_delete_plugin_options')
// --------------------------------------------------------------------------------------

// Delete options table entries ONLY when plugin deactivated AND deleted
function wpasp_delete_plugin_options() {
	delete_option('wpasp_directory_options');
}

// ------------------------------------------------------------------------------
// CALLBACK FUNCTION FOR: register_activation_hook(__FILE__, 'wpasp_add_defaults')
// ------------------------------------------------------------------------------

// Define default option settings
function wpasp_add_defaults() {
	$tmp = get_option('wpasp_directory_options');
    if(!is_array($tmp)) {
		delete_option('wpasp_directory_options'); // so we don't have to reset all the 'off' checkboxes too! (don't think this is needed but leave for now)
		$arr = array(	
			"fieldname" => "Default Value",
		);
		update_option('wpasp_directory_options', $arr);
	}
}

// ------------------------------------------------------------------------------
// CALLBACK FUNCTION FOR: add_action('admin_init', 'wpasp_init' )
// ------------------------------------------------------------------------------
// THIS FUNCTION RUNS WHEN THE 'admin_init' HOOK FIRES, AND REGISTERS YOUR PLUGIN
// SETTING WITH THE WORDPRESS SETTINGS API. YOU WON'T BE ABLE TO USE THE SETTINGS
// API UNTIL YOU DO.
// ------------------------------------------------------------------------------

// Init plugin options to white list our options
function wpasp_init(){
	register_setting( 'wpasp_plugin_options', 'wpasp_directory_options', 'wpasp_validate_options' );
}

// ------------------------------------------------------------------------------
// CALLBACK FUNCTION FOR: add_action('admin_menu', 'wpasp_add_options_page');
// ------------------------------------------------------------------------------

// Add menu page
function wpasp_add_options_page() {
	add_menu_page( 
		'WPA Starter Plugin', 
		'WPA Starter Plugin', 
		'manage_options', 
		'/wpa-starter-plugin/options.php', 
		'wpasp_render_options_page', 
		'dashicons-admin-generic', // or to use an image: plugin_dir_url( __FILE__ ) . '/images/image.png', 
		85 
	);
/*	add_submenu_page( '/chamber-dashboard-business-directory/options.php', 'Export', 'Export', 'manage_options', 'chamber-dashboard-export', 'wpasp_export_form' );
	add_submenu_page( '/chamber-dashboard-business-directory/options.php', 'Import', 'Import', 'manage_options', 'chamber-dashboard-import', 'wpasp_import_form' );*/
}


// ------------------------------------------------------------------------------
// CALLBACK FUNCTION SPECIFIED IN: add_options_page()
// ------------------------------------------------------------------------------
add_action( 'admin_init', 'wpasp_settings_init' );

function wpasp_settings_init(  ) { 

	register_setting( 'pluginPage', 'wpasp_settings' );

	add_settings_section(
		'wpasp_pluginPage_section', 
		__( 'Your section description', 'wpasp' ), 
		'wpasp_settings_section_callback', 
		'pluginPage'
	);

	add_settings_field( 
		'wpasp_text_field_0', 
		__( 'Settings field description', 'wpasp' ), 
		'wpasp_text_field_0_render', 
		'pluginPage', 
		'wpasp_pluginPage_section' 
	);

	add_settings_field( 
		'wpasp_checkbox_field_1', 
		__( 'Settings field description', 'wpasp' ), 
		'wpasp_checkbox_field_1_render', 
		'pluginPage', 
		'wpasp_pluginPage_section' 
	);

	add_settings_field( 
		'wpasp_radio_field_2', 
		__( 'Settings field description', 'wpasp' ), 
		'wpasp_radio_field_2_render', 
		'pluginPage', 
		'wpasp_pluginPage_section' 
	);

	add_settings_field( 
		'wpasp_textarea_field_3', 
		__( 'Settings field description', 'wpasp' ), 
		'wpasp_textarea_field_3_render', 
		'pluginPage', 
		'wpasp_pluginPage_section' 
	);

	add_settings_field( 
		'wpasp_select_field_4', 
		__( 'Settings field description', 'wpasp' ), 
		'wpasp_select_field_4_render', 
		'pluginPage', 
		'wpasp_pluginPage_section' 
	);


}


function wpasp_text_field_0_render(  ) { 

	$options = get_option( 'wpasp_settings' );
	?>
	<input type='text' name='wpasp_settings[wpasp_text_field_0]' value='<?php echo $options['wpasp_text_field_0']; ?>'>
	<?php

}


function wpasp_checkbox_field_1_render(  ) { 

	$options = get_option( 'wpasp_settings' );
	?>
	<input type='checkbox' name='wpasp_settings[wpasp_checkbox_field_1]' <?php checked( $options['wpasp_checkbox_field_1'], 1 ); ?> value='1'>
	<?php

}


function wpasp_radio_field_2_render(  ) { 

	$options = get_option( 'wpasp_settings' );
	?>
	<input type='radio' name='wpasp_settings[wpasp_radio_field_2]' <?php checked( $options['wpasp_radio_field_2'], 1 ); ?> value='1'>
	<?php

}


function wpasp_textarea_field_3_render(  ) { 

	$options = get_option( 'wpasp_settings' );
	?>
	<textarea cols='40' rows='5' name='wpasp_settings[wpasp_textarea_field_3]'> 
		<?php echo $options['wpasp_textarea_field_3']; ?>
 	</textarea>
	<?php

}


function wpasp_select_field_4_render(  ) { 

	$options = get_option( 'wpasp_settings' );
	?>
	<select name='wpasp_settings[wpasp_select_field_4]'>
		<option value='1' <?php selected( $options['wpasp_select_field_4'], 1 ); ?>>Option 1</option>
		<option value='2' <?php selected( $options['wpasp_select_field_4'], 2 ); ?>>Option 2</option>
	</select>

<?php

}


function wpasp_settings_section_callback(  ) { 

	echo __( 'This section description', 'wpasp' );

}


// Render the Plugin options form
function wpasp_render_options_page() {
	?>
	<div class="wrap">
		
		<!-- Display Plugin Icon, Header, and Description -->
		<h2><img src="<?php echo plugin_dir_url( __FILE__ ) . '/images/cdash-32.png'?>"><?php _e('WP Alchemists Starter Plugin Settings', 'wpasp'); ?></h2>


		<div id="main" style="width: 70%; min-width: 350px; float: left;">
			<!-- Beginning of the Plugin Options Form -->
			<form method="post" action="options.php">
				<?php
				settings_fields( 'pluginPage' );
				do_settings_sections( 'pluginPage' );
				submit_button();
				?>
			</form>

		</div><!-- #main -->
		<?php include( plugin_dir_path( __FILE__ ) . '/includes/aside.php' ); ?>
	</div>

	<?php	
}



// Sanitize and validate input. Accepts an array, return a sanitized array.
function wpasp_validate_options($input) {
	$input['sample_field'] =  wp_filter_nohtml_kses($input['sample_field']); 
	// $input['txt_one'] =  wp_filter_nohtml_kses($input['txt_one']); // Sanitize textbox input (strip html tags, and escape characters)
	// $input['textarea_one'] =  wp_filter_nohtml_kses($input['textarea_one']); // Sanitize textarea input (strip html tags, and escape characters)
	return $input;
}

// Display a Settings link on the main Plugins page
function wpasp_plugin_action_links( $links ) {

	$links[] = '<a href="'. esc_url( get_admin_url(null, 'options-general.php?page=wpa-starter-plugin/options.php') ) .'">' . __( 'Settings', 'wpasp' ) . '</a>';
	$links[] = '<a href="http://wpalchemists.com/donate" target="_blank">' . __( 'Donate', 'wpasp' ) . '</a>';

	return $links;
}

?>