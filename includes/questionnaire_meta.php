<div class="my_meta_control">

	<a href="#" class="dodelete-question button"><?php _e('Remove All Questions', 'cdashts'); ?></a>
 
	<?php while($mb->have_fields_and_multi('question')): ?>
	<?php $mb->the_group_open(); ?>

		<div class="question location clearfix">

			<?php $mb->the_field('ask'); ?>
			<label><?php _e('Question', 'cdashts'); ?></label>
			<p><input type="text" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/></p>

			<div class="half left format">
				<?php $mb->the_field('format'); ?>
				<label><?php _e('Format', 'cdashts'); ?></label>
				<?php $selected = ' selected="selected"'; ?>
				<select name="<?php $mb->the_name(); ?>" class="format-select">
					<option value=""></option>
					<option value="text" <?php if ($mb->get_the_value() == "text") echo $selected; ?>><?php _e( 'One-line text', 'cdashts'); ?></option>
					<option value="textarea" <?php if ($mb->get_the_value() == "textarea") echo $selected; ?>><?php _e( 'Multi-line text', 'cdashts'); ?></option>
					<option value="select" <?php if ($mb->get_the_value() == "select") echo $selected; ?>><?php _e( 'Drop-down select', 'cdashts'); ?></option>
					<option value="checkbox" <?php if ($mb->get_the_value() == "checkbox") echo $selected; ?>><?php _e( 'Checkbox(es)', 'cdashts'); ?></option>
					<option value="radio" <?php if ($mb->get_the_value() == "radio") echo $selected; ?>><?php _e( 'Radio buttons', 'cdashts'); ?></option>
				</select>
			</div>

			<?php $display = 'none';
			if( "select" == $mb->get_the_value() || "checkbox" == $mb->get_the_value() || "radio" == $mb->get_the_value() ) {
				$display = 'block';
			} ?>

			<fieldset class="half right option-fieldset" style="display: <?php echo $display; ?>;">
				<legend><?php _e('Options', 'cdashts'); ?></legend>

				<a href="#" class="dodelete-option button"><?php _e('Remove All Options', 'cdashts'); ?></a>
		 
				<?php while($mb->have_fields_and_multi('option')): ?>
				<?php $mb->the_group_open(); ?>
					<?php $mb->the_field('option'); ?>
					<label><?php _e('Option', 'cdashts'); ?></label>
					<p><input type="text" class="option" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/></p>

				<a href="#" class="dodelete button"><?php _e('Remove This Option', 'cdashts'); ?></a>
				<hr />

				<?php $mb->the_group_close(); ?>
				<?php endwhile; ?>
				<p><a href="#" class="docopy-option button"><?php _e('Add Another Option', 'cdashts'); ?></a></p>
			</fieldset>

			<div class="clearfix"></div>

			<div class="half left required">
				<?php $mb->the_field('required'); ?>
				<label><?php _e('Required', 'cdashts'); ?></label>
				<p><input type="checkbox" name="<?php $mb->the_name(); ?>" value="1"<?php if ($mb->get_the_value()) echo ' checked="checked"'; ?>/> <?php _e('This question is required', 'cdashts'); ?></p>
			</div>

			<div class="half right order">
				<?php $mb->the_field('order'); ?>
				<label><?php _e('Order', 'cdashts'); ?></label>
				<p><input type="number" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>"/></p>
			</div>

		<p class="clearfix"><a href="#" class="dodelete button"><?php _e('Remove This question', 'cdashts'); ?></a></p>
 
		</div>
	<?php $mb->the_group_close(); ?>
	<?php endwhile; ?>
	<p><a href="#" class="docopy-question button"><?php _e('Add Another question', 'cdashts'); ?></a></p>


</div>

<script type="text/javascript">
jQuery(document).ready(function ($) {
	$(document).on( 'change', '.format-select', function () {
		console.log('select');
		var val = $(this).val();
		console.log(val);
	    if (val === 'checkbox' || val === 'select' || val === 'radio' ) {
	    	$(this).closest('.question').children('.option-fieldset').show('slow');
	    } else if (val === '' || val === 'text' || val === 'textarea' ) {
	      $(this).closest('.question').children('.option-fieldset').hide('slow');
	    }
	});
	
});
</script>